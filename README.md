# vueApp

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

# Commandline to run workspace


```
cd vue && echo -e "\nWill start at ${server.3000}\n\nInstalling packeges..." && npm install && PORT=3000 npm run start
```
